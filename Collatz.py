#!/usr/bin/env python3

# ------------------
# collatz/Collatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ------------------
# project 1 elad hershkovitz eh27439
'''
This program reads input of two numbers and calculated the max cycle length
between the two
'''


from typing import IO, List


# ------------
# collatz_read
# ------------

def collatz_read(string: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    ranges = string.split()
    return [int(ranges[0]), int(ranges[1])]
# ------------
# collatz_eval
# ------------


def collatz_eval(start: int, end: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    this algorithm uses meta cache to predict max cycle values
    based on pre-determine ranges,
    if no value in the cache fits in the required range it would calculated
    the range by iterating though the range and calculating each number's cycle

    """
    # <your code>
    max_value = 1
    count = 1
    metacache = {
        3: 8,
        6: 9,
        7: 17,
        9: 20,
        18: 21,
        25: 24,
        27: 112,
        54: 113,
        73: 116,
        97: 119,
        129: 122,
        171: 125,
        231: 128,
        313: 131,
        327: 144,
        649: 145,
        703: 171,
        871: 179,
        1161: 182,
        2223: 183,
        2463: 209,
        2919: 217,
        3711: 238,
        6171: 262,
        10971: 268,
        13255: 276,
        17647: 279,
        23529: 282,
        26623: 308,
        34239: 311,
        35655: 324,
        52527: 340,
        77031: 351,
        106239: 354,
        142587: 375,
        156159: 383,
        216367: 386,
        230631: 443,
        410011: 449,
        511935: 470,
        626331: 509,
        837799: 525}

    cache = {1: 1}
    half = (end >> 1) + 1
    cache_flag = 0
    rnum = 0

    if end < start:
        temp = start
        start = end
        end = temp
    if start < half:
        start = half

    for value in metacache:

        if value <= end and value >= start and value > rnum:
            cache_flag = metacache[value]
            rnum = value

    if cache_flag:
        return cache_flag

    for number in range(start, end + 1):
        if rnum > number:
            continue
        value = number
        while number > 1:
            if number in cache:  # check if the cycle was already calculated
                count += cache[number]
                break
            if number % 2 == 0:
                number >>= 1
                count += 1
            else:  # for odd numbers do two operations
                number = (3 * number + 1) >> 1
                count += 2

        cache.update({value: count - 1})  # store the cycle length
        if count > max_value:
            max_value = count
        count = 1

    return max_value

    # -------------
    # collatz_print
    # -------------


def collatz_print(
        writer: IO[str],
        start: int,
        end: int,
        max_cycle: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    writer.write(str(start) + " " + str(end) + " " + str(max_cycle) + "\n")

    # -------------
    # collatz_solve
    # -------------


def collatz_solve(read: IO[str], writer: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for string in read:
        start, end = collatz_read(string)
        max_cycle = collatz_eval(start, end)
        collatz_print(writer, start, end, max_cycle)
