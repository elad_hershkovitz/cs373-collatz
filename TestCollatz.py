#!/usr/bin/env python3

# ----------------------
# collatz/TestCollatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ----------------------

# -------
# imports
# -------
"""
TestCollatz file inherite python's TestCase class
and implement diffrent tests cases
"""
from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve


class TestCollatz(TestCase):
    """
    # read
    input 1 10
    expected output 1 10 20
    """

    def test_read(self):
        """
        read input test
        """
        string = "1 10\n"
        start, end = collatz_read(string)
        self.assertEqual(start, 1)
        self.assertEqual(end, 10)

    def test_eval_1(self):
        """
        given test
        """
        max_cycle = collatz_eval(1, 10)
        self.assertEqual(max_cycle, 20)

    def test_eval_2(self):
        """
        given test
        """
        max_cycle = collatz_eval(100, 200)
        self.assertEqual(max_cycle, 125)

    def test_eval_3(self):
        """
        given test
        """
        max_cycle = collatz_eval(201, 210)
        self.assertEqual(max_cycle, 89)

    def test_eval_4(self):
        """
        given test
        """
        max_cycle = collatz_eval(900, 1000)
        self.assertEqual(max_cycle, 174)

    def test_eval_5(self):
        """
         test 1
        """
        max_cycle = collatz_eval(1, 1000000)
        self.assertEqual(max_cycle, 525)

    def test_eval_6(self):
        """
         test 2
        """
        max_cycle = collatz_eval(87999, 1021)
        self.assertEqual(max_cycle, 351)

    def test_eval_7(self):
        """
         test 3
        """
        max_cycle = collatz_eval(420002, 501302)
        self.assertEqual(max_cycle, 444)

    def test_eval_8(self):
        """
         test 4
        """
        max_cycle = collatz_eval(630440, 800000)
        self.assertEqual(max_cycle, 504)

    def test_eval_9(self):
        """
         test 5
        """
        max_cycle = collatz_eval(89, 410011)
        self.assertEqual(max_cycle, 449)

    def test_eval_10(self):
        """
         test 6
        """
        max_cycle = collatz_eval(837799, 410011)
        self.assertEqual(max_cycle, 525)

    def test_eval_11(self):
        """
         test 7
        """
        max_cycle = collatz_eval(6171, 990999)
        self.assertEqual(max_cycle, 525)

    def test_eval_12(self):
        """
         test 8
        """
        max_cycle = collatz_eval(3498, 940011)
        self.assertEqual(max_cycle, 525)

    def test_eval_13(self):
        """
         test 9
        """
        max_cycle = collatz_eval(99999, 99)
        self.assertEqual(max_cycle, 351)

    def test_eval_14(self):
        """
         test 10
        """
        max_cycle = collatz_eval(3, 93231)
        self.assertEqual(max_cycle, 351)

    def test_print(self):
        """
         output
        """
        writer = StringIO()
        collatz_print(writer, 1, 10, 20)
        self.assertEqual(writer.getvalue(), "1 10 20\n")

    def test_solve(self):
        """
         file test
        """
        reader = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        writer = StringIO()
        collatz_solve(reader, writer)
        self.assertEqual(
            writer.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")


if __name__ == "__main__":  # pragma: no cover
    main()
